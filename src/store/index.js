import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        accessToken: '',
        currentUser: {},
        rootEndpoint: '#',
        apiKey: '#',
        contentType: 'application/json'
    },
    getters: {
        getRootEndpoint(state) {
            return state.rootEndpoint;
        },
        getApiKey(state) {
            return state.apiKey;
        },
        getContentTypeJSON(state) {
            return state.contentType;
        }
    },
    mutations: { },
    actions: { },
    modules: { }
});
