import Vue from 'vue';
import Router from 'vue-router';
import Index from '../pages/index'
import Pinboard from '../pages/pinboard/index'
import Paper from '../pages/paper/index'

Vue.use(Router);

const router =  new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
      {
        path: '/',
        component: Pinboard,
        name: 'Pinboard'
      },
      {
        path: '/paper',
        component: Paper,
        name: 'Paper'
      }
  ]
})

router.beforeEach(async (to, from, next) => {
  next();
});

export default router;
